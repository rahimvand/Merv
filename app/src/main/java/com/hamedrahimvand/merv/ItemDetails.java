package com.hamedrahimvand.merv;

import android.support.annotation.ColorRes;

public class ItemDetails {
    private Integer textColor;
    private Integer backgroundColor;

    public ItemDetails(Integer textColor, Integer backgroundColor) {
        this.textColor = textColor;
        this.backgroundColor = backgroundColor;
    }

    public Integer getTextColor() {
        return textColor;
    }

    public void setTextColor(Integer textColor) {
        this.textColor = textColor;
    }

    public Integer getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Integer backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
}
