package com.hamedrahimvand.merv;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;

public class MervConfig {
    private Integer rotationExpand;
    private Integer rotationCollapse;
    private Drawable sideDrawable;
    private Drawable sideDrawableExpanded;
    private Drawable childDrawableStart;
    private ColorStateList[] colorStateList;
    private int textColor;

    private MervConfig(MervConfigBuilder mervConfigBuilder) {
        this.rotationExpand = mervConfigBuilder.rotationExpand;
        this.rotationCollapse = mervConfigBuilder.rotationCollapse;
        this.sideDrawable = mervConfigBuilder.sideDrawable;
        this.textColor = mervConfigBuilder.textColor;
        this.childDrawableStart = mervConfigBuilder.childDrawableStart;
        this.colorStateList = mervConfigBuilder.colorStateList;
        this.sideDrawableExpanded= mervConfigBuilder.sideDrawableExpanded;
    }

    public Integer getRotationExpand() {
        return rotationExpand;
    }

    public void getChildDrawableStart(Drawable childDrawableStart) {
        this.childDrawableStart = childDrawableStart;
    }

    public Drawable getSideDrawableExpanded() {
        return sideDrawableExpanded;
    }

    public void getColorStateList(ColorStateList[] colorStateList) {
        this.colorStateList = colorStateList;
    }

    public Integer getRotationCollapse() {
        return rotationCollapse;
    }


    public Drawable getSideDrawable() {
        return sideDrawable;
    }


    public int getTextColor() {
        return textColor;
    }

    public Drawable getChildDrawableStart() {
        return childDrawableStart;
    }

    public static class MervConfigBuilder {
        private Integer rotationExpand;
        private Integer rotationCollapse;
        private Drawable sideDrawable;
        private Drawable sideDrawableExpanded;
        private int textColor;
        private Drawable childDrawableStart;
        private ColorStateList[] colorStateList;

        public MervConfigBuilder() {
        }


        public MervConfigBuilder colorStateList(ColorStateList[] colorStateList) {
            this.colorStateList = colorStateList;
            return this;
        }
        public MervConfigBuilder rotationExpand(Integer rotation) {
            this.rotationExpand = rotation;
            return this;
        }

        public MervConfigBuilder rotationCollapse(Integer rotation) {
            this.rotationCollapse = rotation;
            return this;
        }

        public MervConfigBuilder sideDrawable(Drawable sideDrawable) {
            this.sideDrawable = sideDrawable;
            return this;
        }

        public MervConfigBuilder sideDrawableExpanded(Drawable sideDrawableExpanded) {
            this.sideDrawableExpanded = sideDrawableExpanded;
            return this;
        }

        public MervConfigBuilder textColor(@ColorInt int textColor) {
            this.textColor = textColor;
            return this;
        }

        public MervConfigBuilder childDrawableStart (Drawable childDrawableStart ) {
            this.childDrawableStart = childDrawableStart;
            return this;
        }


        public MervConfig build() {
            return new MervConfig(this);
        }


    }



}